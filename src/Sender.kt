import java.net.DatagramPacket
import java.net.InetAddress
import java.net.DatagramSocket
import kotlin.system.exitProcess


fun main(args: Array<String>) {

    val address = InetAddress.getByName("localhost")
    val msg = "yoooooooo"


    val socket = DatagramSocket()
    val send = { msg: String ->
        val buf = msg.toByteArray()
        val packet = DatagramPacket(buf, buf.size, address, 4445)
        socket.send(packet)
    }


    val commands = arrayOf("toggle", "play", "pause", "open", "quit")
    println("listening for commands: \n${commands.joinToString("\n")}")
    do {
        val line = readLine()
        line?.let {
            if (commands.contains(it)) {
                send(it)
                if(it == "quit"){
                   println("bye!")
                   exitProcess(0)
                }
            } else {
                println("unknown command: $line")
            }
        }
    } while (true)

    //val reader = Scanner(System.`in`)
    //println("type :q<ENTER> to quit")
    //println("enter you command")
    //
    //reader.next("play")
    //send("play")
    //reader.next(":q")
    //socket.close()
    println("quit")
}

