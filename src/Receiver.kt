import java.net.DatagramPacket
import java.net.DatagramSocket
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

data class MpvConfig(val videoFile: String, val serverPath: String)
data class MpvControl(
        val play: ProcessBuilder,
        val pause: ProcessBuilder,
        val toggle: ProcessBuilder,
        val quit: ProcessBuilder
)

val runProcess = { builder: ProcessBuilder ->
    val process = builder.start()
    process.inputStream.reader(Charsets.UTF_8).use {
        println("stdin: ${it.readText()}")
    }
    process.waitFor(10, TimeUnit.SECONDS)
}

val files = listOf(
        "/home/gbr/dora/testvideo/test.mp4",
        "/home/gbr/dora/testvideo/test.mp4"
)

fun main(args: Array<String>) {

    val os = System.getProperty("os.name")!!.toLowerCase()

    val mpvConfigs = files.mapIndexed { index, f ->
        MpvConfig(videoFile = f, serverPath = "/tmp/mpv-ipc-$index")
    }

    // start mpv instances
    mpvConfigs.forEach {
        println("staring mpv with: ${it.videoFile}, with server at ${it.serverPath}")
        val p = when (os) {
            "windows" ->
                throw Exception("do not know how to start mpv on windows")
            "mac", "linux" -> ProcessBuilder("mpv", it.videoFile, "--pause", "--loop-file", "--input-ipc-server=${it.serverPath}")
            else -> {
                throw Exception("do not know os: $os")
            }
        }

        val t = Thread {
            runProcess(p)
        }

        t.start()
    }

    val mpvCmd = { cmd: String, server: String ->
        when (os) {
            "windows" -> {
                throw Exception("not implemented yet")
            }
            "mac", "linux" -> {
                val p = "echo '$cmd' | socat - $server"
                println("making mpvCmd: $p")
                ProcessBuilder("/bin/sh", "-c", p)
            }
            else -> {
                throw Exception("do not know os: $os")
            }
        }
    }

    val controls = mpvConfigs.map {
        MpvControl(
                play = mpvCmd("set pause no", it.serverPath),
                pause = mpvCmd("set pause yes", it.serverPath),
                toggle = mpvCmd("cycle pause", it.serverPath),
                quit = mpvCmd("quit", it.serverPath)
        )
    }

    val serverSocket = DatagramSocket(4445)
    while (true) {
        val buf = ByteArray(256)
        val packet = DatagramPacket(buf, buf.size)
        serverSocket.receive(packet)
        val address = packet.address
        val port = packet.port
        println(address)
        println(port)
        val otherpacket = DatagramPacket(buf, buf.size, address, port)
        val content = otherpacket.data.filter { it.toInt() != 0 }.toByteArray()
        val received = String(content).trim()
        println("received: $received")
        when (received) {
            "pause" -> {
                controls.forEach {
                    runProcess(it.pause)
                }
            }
            "play" -> {
                controls.forEach {
                    runProcess(it.play)
                }
            }
            "toggle" -> {
                controls.forEach {
                    runProcess(it.toggle)
                }
            }
            "quit" -> {
                serverSocket.close()
                controls.forEach {
                    runProcess(it.quit)
                }
                exitProcess(0)
            }
        }
    }
}